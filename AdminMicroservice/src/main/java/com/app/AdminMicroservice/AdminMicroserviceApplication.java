package com.app.AdminMicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2
public class AdminMicroserviceApplication 
{

	public static void main(String[] args) 
	{
		SpringApplication.run(AdminMicroserviceApplication.class, args);
	}

	//Swagger
	@Bean
	public Docket adminApi() 
	{
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.app.AdminMicroservice")).build();
	}

	//RestTemplate Bean Creation
	@Bean
	public RestTemplate restTemplate() 
	{
		return new RestTemplate();
	}

}
