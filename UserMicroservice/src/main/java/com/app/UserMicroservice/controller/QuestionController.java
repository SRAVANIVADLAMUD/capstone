package com.app.UserMicroservice.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.app.UserMicroservice.dto.AskQuestion;
import com.app.UserMicroservice.model.Question;
import com.app.UserMicroservice.repository.UserRepository;
import com.app.UserMicroservice.service.QuestionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class QuestionController
{
	@Autowired
	UserRepository userRepository;

	@Autowired
	QuestionService questionService;

	// Swagger
	@ApiOperation(value = "Question List", notes = "This method will retrieve list from database", nickname = "getQuestion")
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Server error"),@ApiResponse(code = 404, message = "Question not found"),
	@ApiResponse(code = 200, message = "Successful retrieval", response = Question.class, responseContainer = "List") })

	//Taking Questions from User
	@RequestMapping("/questionForm")
	public ModelAndView question(HttpServletRequest request) 
	{
		request.setAttribute("userobj", "USER_QUESTION");
		return new ModelAndView("userWelcomePage");
	}

	//Posting The Questions
	@RequestMapping("/askQuestion")
	public ModelAndView askQuestion(@ModelAttribute AskQuestion askQuestion, BindingResult bindingResult,
			HttpServletRequest request) 
	{
		if (userRepository.findByUserId(askQuestion.getUserId()) != null) 
		{
			request.setAttribute("question", questionService.askQuestion(askQuestion));
			request.setAttribute("mode", "MODE_ASK");
			return new ModelAndView("userWelcomePage");
		} else 
		{
			request.setAttribute("error", "UserID Not Exist,Please Try Again");
			request.setAttribute("userobj", "USER_QUESTION");
			return new ModelAndView("userWelcomePage");
		}
	}

	//Approved Questions for User
	@RequestMapping("/approvedQuestionData")
	public ModelAndView approvedQuestions(HttpServletRequest request) 
	{
		List<Question> ulist = questionService.getApprovedQuestions();
		request.setAttribute("mode", "QUESTION_DATA");
		return new ModelAndView("adminOperations", "questiondata", ulist);
	}
	
	//Approved Questions for Admin with Delete Operation
	@RequestMapping("/approvedQuestions")
	public ModelAndView approved(HttpServletRequest request) 
	{
		List<Question> ulist = questionService.getApprovedQuestions();
		request.setAttribute("mode", "QUESTION_ADMIN");
		return new ModelAndView("adminOperations", "questiondata", ulist);
	}

	@RequestMapping("/unApprovedQuestions")
	public ModelAndView showAllQuestions(HttpServletRequest request)
	{
		List<Question> ulist = questionService.getUnApprovedQuestions();
		request.setAttribute("mode", "UNAPPROVED_QUESTIONS");
		return new ModelAndView("adminOperations", "questiondata", ulist);
	}

	@RequestMapping("/approveQuestion/{questionId}")
	public ModelAndView approveQuestion(@PathVariable("questionId") int questionId, HttpServletRequest request) 
	{
		request.setAttribute("question", questionService.approveQuestion(questionId));
		request.setAttribute("mode", "APPROVE_QUESTION");
		return new ModelAndView("adminOperations");
	}

	//Delete Inappropriate Operation Code Implementation
	@RequestMapping("/deleteInappropriateQuestions/{questionId}")
	public ModelAndView deleteQuestions(@PathVariable("questionId") int questionId, HttpServletRequest request) 
	{
		questionService.deleteQuestion(questionId);
		List<Question> ulist = questionService.getUnApprovedQuestions();
		request.setAttribute("mode", "UNAPPROVED_QUESTIONS");
		return new ModelAndView("adminOperations", "questiondata", ulist);
	}
	
	@RequestMapping("/deleteInappropriateQuestionsApproved/{questionId}")
	public ModelAndView delete(@PathVariable("questionId") int questionId, HttpServletRequest request) 
	{
		questionService.deleteQuestion(questionId);
		List<Question> ulist = questionService.getApprovedQuestions();
		request.setAttribute("mode", "QUESTION_ADMIN");
		return new ModelAndView("adminOperations", "questiondata", ulist);
	}
	
	//Search Functionality
	@RequestMapping("/searchForm")
	public ModelAndView search(HttpServletRequest request) 
	{
		request.setAttribute("mode", "SEARCH_QUESTION");
		return new ModelAndView("userWelcomePage");
	}
	
	@RequestMapping("/searchByQuestion")
	public ModelAndView ByQuestion(@RequestParam String question, HttpServletRequest request) throws IOException
	{
		List<Question> slist = questionService.searchByQuestion(question);
		request.setAttribute("mode", "QUESTION_DATA");
		return new ModelAndView("userWelcomePage", "questiondata", slist);
	}
	
	@RequestMapping("/searchTopicForm")
	public ModelAndView searchTopic(HttpServletRequest request) 
	{
		request.setAttribute("mode", "SEARCH_TOPIC");
		return new ModelAndView("userWelcomePage");
	}
	
	@RequestMapping("/searchByTopic")
	public ModelAndView ByTopic(@RequestParam String questionTopic, HttpServletRequest request) throws IOException
	{
		List<Question> slist = questionService.searchByTopic(questionTopic);
		request.setAttribute("mode", "QUESTION_DATA");
		return new ModelAndView("userWelcomePage", "questiondata", slist);
	}
	
}
