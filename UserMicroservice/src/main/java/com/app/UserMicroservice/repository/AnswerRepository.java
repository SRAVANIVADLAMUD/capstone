package com.app.UserMicroservice.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.UserMicroservice.model.Answer;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Integer> 
{
	//Queries
	@Query("Select ansobj from Answer ansobj where ansobj.isApproved = false")
	public List<Answer> findByIsApproved();
	
	@Query("Select ansobj from Answer ansobj where ansobj.isApproved = true")
	public List<Answer> findByIsApprovedAnswers();
	
	@Query("from Answer where question.questionId = ?1 and isApproved = true")
	public List<Answer> getAnswer(int questionId);

	public Answer findByAnswerId(int answerId);
	
}
