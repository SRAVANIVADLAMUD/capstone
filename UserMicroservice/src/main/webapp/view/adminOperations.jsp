<!DOCTYPE html >
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="sat, 01 Dec 2001 00:00:00 GMT">
<title>Welcome</title>
<link href="static/css/bootstrap.min.css" rel="stylesheet">
<link href="static/css/style.css" rel="stylesheet">

</head>
<style>
th {
	background-color: black;
	color: white;
	text-align: center;
	text-size: 5px;
}
</style>
<body>
	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/adminHome" class="navbar-brand"><font color=white
				size=4px>Welcome|Admin</font></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/logout"><font color=white size=4px>Logout</font></a></li>
				</ul>
			</div>
		</div>
	</div>
	<c:choose>

		<c:when test="${mode=='ALL_USERS' }">
			<div class="container text-center" id="tasksDiv">
				<h3>All Users</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>User Id</th>
								<th>User Name</th>
								<th>Password</th>
								<th>Delete</th>
								<th>Update</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="userdata" items="${userdata}" varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${userdata.userId}</td>
									<td>${userdata.username}</td>
									<td>${userdata.password}</td>
									<td><a href="/delete-user?userId=${userdata.userId }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
									<td><a href="/edit-user?userId=${userdata.userId }"><span
											class="glyphicon glyphicon-pencil"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>

		<c:when test="${mode=='MODE_UPDATE' }">
			<div class="container text-center">
				<h3>Update User</h3>
				<hr>
				<form class="form-horizontal" method="POST"
					action="/saveUserUpdation">
					<div class="form-group">
						<label class="control-label col-md-3"></label>
						<div class="col-md-7">
							<input type="hidden" class="form-control" name="userId"
								value="${user.userId }" placeholder="MobileNo" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">username</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="username"
								value="${user.username }" placeholder="Username" required />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">password</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="password"
								value="${user.password }" placeholder="Password" required />
						</div>
					</div>
					<div class="form-group ">
						<input type="submit" class="btn btn-primary" value="Update" />
					</div>
				</form>
			</div>
		</c:when>

		<c:when test="${mode=='QUESTION_DATA' }">
			<div class="container text-center" id="tasksDiv">
				<h3>Approved Questions</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Question Id</th>
								<th>Topic</th>
								<th>Question</th>
								<th>Approved Status</th>
								<th>Show Answer</th>
								<th>Post Answer</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="questiondata" items="${questiondata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${questiondata.questionId}</td>
									<td>${questiondata.questionTopic}</td>
									<td>${questiondata.question}</td>
									<td>${questiondata.isApproved}</td>
									<td><a
										href="/getAnswers?questionId=${questiondata.questionId}">Answer</a></td>
									<td><a href="/answerForm">Post Answer</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>

		<c:when test="${mode=='QUESTION_ADMIN' }">
			<div class="container text-center" id="tasksDiv">
				<h3>Approved Questions</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Question Id</th>
								<th>Topic</th>
								<th>Question</th>
								<th>Approved Status</th>
								<th>Show Answer</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="questiondata" items="${questiondata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${questiondata.questionId}</td>
									<td>${questiondata.questionTopic}</td>
									<td>${questiondata.question}</td>
									<td>${questiondata.isApproved}</td>
									<td><a
										href="/getAnswerData?questionId=${questiondata.questionId}">Answer</a></td>
									<td><a
										href="/delete-InappropriateQuestion?questionId=${questiondata.questionId }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode=='UNAPPROVED_QUESTIONS' }">
			<div class="container text-center" id="tasksDiv">
				<h3></h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Question Id</th>
								<th>Topic</th>
								<th>Question</th>
								<th>Approved Status</th>
								<th>Approve</th>
								<th>Approved Questions</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="questiondata" items="${questiondata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${questiondata.questionId}</td>
									<td>${questiondata.questionTopic}</td>
									<td>${questiondata.question}</td>
									<td>${questiondata.isApproved}</td>
									<td><a
										href="/approveQuestions?questionId=${questiondata.questionId}">Approve</a></td>
									<td><a href="/showApprovedQuestions">Approved Questions</a></td>
									<td><a
										href="/delete-question?questionId=${questiondata.questionId }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>

		<c:when test="${mode=='ANSWER_ADMIN' }">
			<div class="container text-center" id="tasksDiv">
				<h3>ANSWER</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Answer Id</th>
								<th>User</th>
								<th>Question</th>
								<th>Answer</th>
								<th>Approved Status</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="answerdata" items="${answerdata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${answerdata.answerId}</td>
									<td>${answerdata.answerUser}</td>
									<td>${answerdata.question}</td>
									<td>${answerdata.answer}</td>
									<td>${answerdata.isApproved}</td>
									<td><a
										href="/delete-InappropriateAnswer?answerId=${answerdata.answerId }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>

		<c:when test="${mode=='UNAPPROVED_ANSWERS' }">
			<div class="container text-center" id="tasksDiv">
				<h3>ANSWER</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Answer Id</th>
								<th>User</th>
								<th>Question</th>
								<th>Answer</th>
								<th>Approved Status</th>
								<th>Approve</th>
								<th>Approved Questions</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="answerdata" items="${answerdata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${answerdata.answerId}</td>
									<td>${answerdata.answerUser}</td>
									<td>${answerdata.question}</td>
									<td>${answerdata.answer}</td>
									<td>${answerdata.isApproved}</td>
									<td><a
										href="/approveAnswers?answerId=${answerdata.answerId}">Approve</a></td>
									<td><a href="/showApprovedAnswers">Approved Answers</a></td>
									<td><a
										href="/delete-answer?answerId=${answerdata.answerId }"><span
											class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode=='QUESTION_ANSWER' }">
			<div class="container text-center" id="tasksDiv">
				<h3>ANSWER</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>SNo.</th>
								<th>Answer Id</th>
								<th>User</th>
								<th>Question</th>
								<th>Answer</th>
								<th>Approved Status</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="answerdata" items="${answerdata}"
								varStatus="status">
								<tr>
									<td>${status.index + 1}</td>
									<td>${answerdata.answerId}</td>
									<td>${answerdata.answerUser}</td>
									<td>${answerdata.question}</td>
									<td>${answerdata.answer}</td>
									<td>${answerdata.isApproved}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>

		<c:when test="${mode=='APPROVE_ANSWER'}">
			<div class="container text-center">
				<h3>Approved Successfully!!</h3>
				<hr>
			</div>
		</c:when>

		<c:when test="${mode=='APPROVE_QUESTION'}">
			<div class="container text-center">
				<h3>Approved Successfully!!</h3>
				<hr>
			</div>
		</c:when>

	</c:choose>
	<script src="static/js/jquery-1.11.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
</body>
</html>